FROM alpine
 
RUN apk add --update nodejs \
        && apk add --update npm \
        && apk add sqlite \
        && apk add python3 \
        && rm -rf /var/cache/apk/*

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .

CMD [ "node", "index.js" ]
