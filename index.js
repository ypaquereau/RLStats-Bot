const Discord = require('discord.js');
const request = require('request');
const sq = require('sqlite3');
require('dotenv').config();


const client = new Discord.Client();
const db = new sq.Database('database.db');

db.serialize(function () {
    db.run('CREATE TABLE IF NOT EXISTS players (id INTEGER PRIMARY KEY AUTOINCREMENT, discord_id TEXT UNIQUE, platform TEXT NOT NULL, player_id TEXT NOT NULL)');
})


const prefix = '!';
const code = "```";

client.login(process.env.BOT_TOKEN).then(() => {
    client.on("message", function (message) {
        if (message.author.bot) return;
        if (!message.content.startsWith(prefix)) return;

        const commandBody = message.content.slice(prefix.length);
        const args = commandBody.split(' ');

        const command = args.shift().toLowerCase();

        try {
            if (command === 'stats') {
                let infos = {};

                if (args.length === 0) {
                    let sql = `SELECT * FROM players where discord_id = ?`;

                    db.all(sql, [message.author.id], (err, rows) => {
                        if (err) {
                            console.log(err);
                            message.reply(`${code}\nAn error occurred, try later${code}`);
                            return;
                        }
                        if (rows.length === 0) {
                            message.reply(`${code}\nCan't find player, use '!set' command first${code}`);
                            return;
                        }

                        infos.platform = rows[0].platform;
                        infos.player_id = rows[0].player_id;

                        dataCommand(infos, message);
                    });
                }

                else if (args.length !== 2) {
                    message.reply(`${code}\nFormat: !stats platform player${code}`);
                    return;
                }

                else {
                    infos.platform = args[0];
                    infos.player_id = args[1];

                    dataCommand(infos, message);
                }                       
            }

            if (command === 'help') {
                message.reply(`${code}\nFormat: !stats platform player${code}`);
            }

            if (command === 'sardvstiifux') {
                let tiifuxId = '76561198929774768';
                let sardId = '76561198004707800';

                let diff = 0;

                request(`https://api.tracker.gg/api/v2/rocket-league/standard/profile/steam/${tiifuxId}`, function (
                    error,
                    response,
                    body
                ) {
                    if (!JSON.parse(body)) {
                        message.reply(`${code}\nAn error occurred, try later${code}`);
                        console.log('json error');
                        console.log(body);

                        return;
                    }

                    let data = JSON.parse(body);

                    if (data.errors !== undefined) {
                        if (data.errors[0].code === 'CollectorResultStatus::NotFound')
                            message.reply(`${code}\nPlayer '${args[1]}' not found${code}`);
                        else {
                            message.reply(`${code}\nAn error occurred, try later${code}`);
                            console.log(data.errors);
                        }
                        return;
                    }

                    data = getData(data);

                    request(`https://api.tracker.gg/api/v2/rocket-league/standard/profile/steam/${sardId}`, function (
                        error,
                        response,
                        bodySard
                    ) {
                        let dataSard = JSON.parse(bodySard);

                        if (dataSard.errors !== undefined) {
                            if (dataSard.errors[0].code === 'CollectorResultStatus::NotFound')
                                message.reply(`${code}\nPlayer '${args[1]}' not found${code}`);
                            else {
                                message.reply(`${code}\nAn error occurred, try later${code}`);
                                console.log(data.errors);
                            }
                            return;
                        }

                        dataSard = getData(dataSard);

                        diff = data.duo.mmr - dataSard.duo.mmr;

                        let result;

                        if (diff > 0) {
                            result = `${code}\nTiifux is ${diff}pts ahead of Sardoche in 2vs2\n\nTiifux: ${data.duo.mmr}\nSardoche: ${dataSard.duo.mmr}${code}`;
                        } else {
                            diff *= -1;
                            result = `${code}\nSardoche is ${diff}pts ahead of Tiifux in 2vs2\n\nTiifux: ${data.duo.mmr}\nSardoche: ${dataSard.duo.mmr}${code}`;
                        }

                        message.reply(result);
                    });
                });
            }

            if (command === 'set') {
                if (args.length !== 2) {
                    message.reply(`${code}\nFormat: !set platform player${code}`);
                    return;
                }

                let infos = {
                    discord_id: message.author.id,
                    platform: args[0],
                    player_id: args[1]
                };

                db.run(`INSERT INTO players (discord_id, platform, player_id)
                            VALUES(?, ?, ?) 
                            ON CONFLICT(discord_id) 
                            DO UPDATE SET platform=excluded.platform, player_id = excluded.player_id;`, [infos.discord_id, infos.platform, infos.player_id]
                    , function (err) {
                        if (err) {
                            console.log(err);
                            message.reply(`${code}\nAn error occurred, try later${code}`);
                        }
                        else {
                            message.reply(`${code}\nPlayer successfully saved !${code}`);
                        }
                    });
            }
        } catch (error) {
            message.reply(`${code}\nAn error occurred, try later${code}`);
            console.log(error);
            return;
        }
    });
});

function getData(json, message) {
    let data = {
        mode: [],
        rank: [],
        mmr: [],
        games: []
    };

    let availableModes = ['Ranked Duel 1v1', 'Ranked Doubles 2v2', 'Ranked Standard 3v3', 'Tournament Matches'];
    let playlists = json.data.segments;

    playlists.forEach(playlist => {
        let mode = playlist.metadata.name;
        let estimate = true;

        if (availableModes.indexOf(mode) !== -1) {
            let rank;
            let estimated = playlist.stats.tier.metadata.estimated;
            let division = null;

            if (estimated !== undefined && estimated === true)
                rank = 'Unranked';
            else {
                rank = playlist.stats.tier.metadata.name;
                if (playlist.stats.division.metadata.name !== undefined)
                    division = playlist.stats.division.metadata.name;
            }


            let countGames = playlist.stats.matchesPlayed.value;

            (countGames > 1) ? countGames += ` games` : countGames += ` game`;

            data.rank.push(getRank(rank, message, division));
            data.mmr.push(`${playlist.stats.rating.value} (${countGames})`);

            switch (mode) {
                case availableModes[0]:
                    data.mode.push('Solo');
                    break;
                case availableModes[1]:
                    data.mode.push('Duo');
                    break;
                case availableModes[2]:
                    data.mode.push('Trio');
                    break;
                case availableModes[3]:
                    data.mode.push('Tournament');
                    break;
            }
        }
    });

    return data;
}

function getRank(fullRank, message, division) {
    let name;
    let rank;

    switch (fullRank) {
        case 'Unranked':
            name = 'unranked';
            rank = 'Unranked';
            break;
        case 'Supersonic Legend':
            name = 'supersonic_legend';
            rank = 'SSL';
            break;
        case 'Grand Champion I':
            name = 'grand_champion1';
            rank = 'GC1';
            break;
        case 'Grand Champion II':
            name = 'grand_champion2';
            rank = 'GC2';
            break;
        case 'Grand Champion III':
            name = 'grand_champion3';
            rank = 'GC3';
            break;
        case 'Champion I':
            name = 'champion1';
            rank = 'C1';
            break;
        case 'Champion II':
            name = 'champion2';
            rank = 'C2';
            break;
        case 'Champion III':
            name = 'champion3';
            rank = 'C3';
            break;
        case 'Diamond I':
            name = 'diamond1';
            rank = 'D1';
            break;
        case 'Diamond II':
            name = 'diamond2';
            rank = 'D2';
            break;
        case 'Diamond III':
            name = 'diamond3';
            rank = 'D3';
            break;
        case 'Platinum I':
            name = 'platinum1';
            rank = 'P1';
            break;
        case 'Platinum II':
            name = 'platinum2';
            rank = 'P2';
            break;
        case 'Platinum III':
            name = 'platinum3';
            rank = 'P3';
            break;
        case 'Gold I':
            name = 'gold1';
            rank = 'G1';
            break;
        case 'Gold II':
            name = 'gold2';
            rank = 'G2';
            break;
        case 'Gold III':
            name = 'gold3';
            rank = 'G3';
            break;
        case 'Silver I':
            name = 'silver1';
            rank = 'S1';
            break;
        case 'Silver II':
            name = 'silver2';
            rank = 'S2';
            break;
        case 'Silver III':
            name = 'silver3';
            rank = 'S3';
            break;
        case 'Bronze I':
            name = 'bronze1';
            rank = 'B1';
            break;
        case 'Bronze II':
            name = 'bronze2';
            rank = 'B2';
            break;
        case 'Bronze III':
            name = 'bronze3';
            rank = 'B3';
            break;
        default:
            return '';
    }

    let emoji = message.guild.emojis.cache.find(emoji => emoji.name === name);

    if (division !== null)
        return `${emoji} ${rank} ${division}`;
    else
        return `${emoji} ${rank}`;
}

function dataCommand(infos, message) {
    const platforms = ['pc', 'xbox', 'play'];
    let platform = '';

    if (!platforms.includes(infos.platform.toLowerCase())) {
        message.reply(`${code}\nAvailable platforms: pc, xbox, play${code}`);
        return;
    } else {
        switch (infos.platform.toLowerCase()) {
            case 'pc':
                platform = 'steam';
                break
            case 'xbox':
                platform = 'xbl';
                break
            case 'play':
                platform = 'psn';
                break
        }
    }

    request(`https://api.tracker.gg/api/v2/rocket-league/standard/profile/${platform}/${infos.player_id}`, function (
        error,
        response,
        body
    ) {
        let data = JSON.parse(body);

        if (data.errors !== undefined) {
            if (data.errors[0].code === 'CollectorResultStatus::NotFound')
                message.reply(`${code}\nPlayer '${infos.players_id}' not found${code}`);
            else {
                message.reply(`${code}\nAn error occurred, try later${code}`);
                console.log(data.errors);
            }
            return;
        }

        let playerName = data.data.platformInfo.platformUserHandle;
        let uniqueId = data.data.platformInfo.platformUserIdentifier;
        let playerPlatform = data.data.platformInfo.platformSlug;
        let avatarUrl = data.data.platformInfo.avatarUrl;

        let embedMessage = new Discord.MessageEmbed()
            .setColor('#31C2F5')
            .setAuthor(playerName, avatarUrl, `https://rocketleague.tracker.network/rocket-league/profile/${playerPlatform}/${uniqueId}/overview`)
            .setTimestamp()
            ;

        data = getData(data, message);

        embedMessage.addFields(
            { name: 'Mode', value: data.mode, inline: true },
            { name: 'Rank', value: data.rank, inline: true },
            { name: 'MMR', value: data.mmr, inline: true },
        );

        message.channel.send(embedMessage);
    });
}
